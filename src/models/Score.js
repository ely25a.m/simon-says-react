const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const  { Schema } = mongoose;

const ScoreSchema = new Schema({
    score: {type: String, required: true},
    date: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Score', UserSchema);