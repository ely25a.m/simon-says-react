const SQUARE_WIDTH_INT = 200;

export const CONSTANTS = {
   boardArr: [0, 1, 2, 3, 4, 5, 6, 7, 8 ],
   colorMap: ['black', 'gray', 'white', 'red', 'blue', 'green', 'yellow','orange'],
   INTERVAL: 500,
   INTERVAL_SPACING: 200,
   SQUARE_WIDTH_INT: 200,
   SQUARE_WIDTH: SQUARE_WIDTH_INT + 'px',
   ROW_WIDTH: SQUARE_WIDTH_INT * 4.2 + 'px',
   ROW_HEIGHT: SQUARE_WIDTH_INT * 2 + 16 + 'px',
};
