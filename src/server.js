const express = require('express');
const engine = require('react-engine');
const path = require('path');

const app = express();

app.use(express.static(__dirname + '/public'));

app.engine('.jsx', engine.server.create({
  reactRoutes: path.join(__dirname, 'App.jsx')
}));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jsx');
app.set('view', engine.expressView);

app.get('/', (req, res) => res.render('App'));

app.listen(3000);